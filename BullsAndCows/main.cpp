#include <iostream>
#include <string>


void			PrintIntro();
void			PlayGame();
std::string		GetGuess();

//app entry
int main()
{
	PrintIntro();
	PlayGame();
	return 0;
}

void PrintIntro()
{
	constexpr int WORD_LENGHT = 5;
	std::cout << "Welcome to Bulls and Cows" << std::endl;
	std::cout << "Can you guess the " << WORD_LENGHT << " letter isogram I'm thinking of?" << std::endl << std::endl;
	return;
}

void PlayGame()
{
	constexpr int NUMBER_OF_TURNS = 5;
	for (int count = 1; count <= NUMBER_OF_TURNS; count++)
	{
		std::string Guess = GetGuess();
		std::cout << "Your guess was: " << Guess << std::endl;
	}
}

std::string GetGuess()
{
	std::cout << "Enter your guess: ";
	std::string Guess = "";
	std::getline(std::cin, Guess);
	return Guess;
}